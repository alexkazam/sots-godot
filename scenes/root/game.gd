extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	registerMapLinks()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func registerMapLinks():
	var mapLinkNodes = get_tree().get_nodes_in_group("MAP_LINK")
	for mapLinkNode in mapLinkNodes:
		mapLinkNode.connect("map_transition", self, "_on_map_transition")
	
func map_transition(targetPackedScene:PackedScene, uniqueName:String):
	var player = $Player
	var map = $TileMap
	remove_child(map)
	map.queue_free()
	var rootSceneNode = targetPackedScene.instance()
	self.add_child(rootSceneNode)
	self.move_child(rootSceneNode, 0)
	var mapLinkNodes = get_tree().get_nodes_in_group("MAP_LINK")

	for mapLinkNode in mapLinkNodes:
		if uniqueName == mapLinkNode.uniqueName:
			
			player.set_global_position(mapLinkNode.get_node("SpawnPoint").get_global_position())
			player.velocity = Vector2(0, 0)
			player.set_camera_limits()
			
	registerMapLinks()

func _on_map_transition(targetPackedScene:PackedScene, uniqueName:String):
	call_deferred("map_transition", targetPackedScene, uniqueName)