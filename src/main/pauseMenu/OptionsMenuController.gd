extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_OptionsMenu_menu_item_selected(command, labelNode):
	if command == "modern":
		DisplayManager.resetColor()
	if command == "classic":
		DisplayManager.setColorGreen()
	if command == "back":
		self.queue_free()
