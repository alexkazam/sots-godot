extends Node2D

onready var optionsMenuScene:PackedScene = preload("res://src/main/pauseMenu/OptionsMenuController.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_MainMenu_menu_item_selected(command, labelNode:Label):
	if command == "options":
		var optionMenu:Node2D = optionsMenuScene.instance()
		self.get_parent().add_child(optionMenu)
		optionMenu.set_global_position(labelNode.get_global_position())
		optionMenu.global_position.x += labelNode.rect_size.x
		optionMenu.global_position.y -= 8



