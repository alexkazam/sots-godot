extends KinematicBody2D

export (int) var speed = 96

const interactableClass = preload("res://src/interactables/Interactable.gd")

var velocity = Vector2()
var acceptingInput = true;

onready var sprite:AnimatedSprite = $AnimatedSprite
onready var interactionArea:Area2D = $InteractionArea

# Called when the node enters the scene tree for the first time.
func _ready():
	set_camera_limits()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _unhandled_input(event):
	velocity = Vector2(0, 0)
	if event.is_action_pressed("a"):
		interact()
		return
	if Input.is_action_pressed('right'):
		velocity.x = 1
		interactionArea.position = Vector2(16, 0)
		interactionArea.scale = Vector2(1.5, 0.5)
	if Input.is_action_pressed('left'):
		velocity.x = -1
		interactionArea.position = Vector2(-16, 0)
		interactionArea.scale = Vector2(1.5, 0.5)
	if Input.is_action_pressed('down'):
		velocity.y = 1
		interactionArea.position = Vector2(0, 16)
		interactionArea.scale = Vector2(0.5, 1.5)
	if Input.is_action_pressed('up'):
		velocity.y = -1
		interactionArea.position = Vector2(0, -16)
		interactionArea.scale = Vector2(0.5, 1.5)
	velocity = velocity.normalized() * speed
	get_tree().set_input_as_handled()

func _physics_process(delta):
	if velocity.y < 0:
		sprite.play("walk-up")
	elif velocity.y > 0:
		sprite.play("walk-down")
	elif velocity.x > 0:
		sprite.play("walk-right")
	elif velocity.x < 0:
		sprite.play("walk-left")
	else:
		sprite.stop()
		sprite.frame = 0
	move_and_slide(velocity)

	
func set_camera_limits():
	var camera = $Camera2D
	var tileMap = get_parent().get_node("TileMap")
	if tileMap != null:
		var map_limits = tileMap.get_used_rect()
		var map_cellsize = tileMap.cell_size
		camera.limit_left = map_limits.position.x * map_cellsize.x
		camera.limit_right = map_limits.end.x * map_cellsize.x
		camera.limit_top = map_limits.position.y * map_cellsize.y
		camera.limit_bottom = map_limits.end.y * map_cellsize.y
	camera.align()
	camera.reset_smoothing()


func interact():
	var bodies:Array = interactionArea.get_overlapping_bodies()
	if bodies != null && bodies.size() > 0:
		var closestBody
		var closestDistance
		for body in bodies:
			var distance = self.global_position.distance_to(body.global_position)
			if body != self && (closestDistance == null || distance < closestDistance):
				if body is interactableClass:
					closestDistance = distance
					closestBody = body
		if closestBody != null:
			closestBody.interact(self);
			velocity = Vector2()
