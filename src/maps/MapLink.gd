extends Node2D


signal map_transition

export(String, FILE, "*.tscn,*.scn") var targetScenePath
export var uniqueName:String
onready var packedScene:PackedScene = ResourceLoader.load(targetScenePath)

func _on_Area2D_body_entered(body):
	if body is KinematicBody2D:
		emit_signal("map_transition", packedScene, uniqueName)
