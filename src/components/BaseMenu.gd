extends PanelContainer

export var allowBackCommand:bool = false

onready var basicLabelScene = preload("res://src/components/BasicLabel.tscn")
onready var containerNode = $VBoxContainer

var cursorIndex = 0
var cursorSprite

signal menu_item_selected(command, labelNode)


# Called when the node enters the scene tree for the first time.
func _ready():
	cursorSprite = get_node("CursorSprite")
	#addMenuItem("Test One", "1")
	#addMenuItem("Test Two", "2")
	#addMenuItem("Test Three", "3")
	cursorSprite.position.x = 0
	cursorSprite.position.y = 8

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func addMenuItem(labelText: String, command: String):
	var vc = get_node('VBoxContainer')
	var labelNode = basicLabelScene.instance();
	labelNode.text = labelText
	labelNode.command = command
	labelNode.rect_size.y = 8
	vc.add_child(labelNode)

	#self.rect_size.y = vc.rect_size.y + 16
	


func _unhandled_input(event):
	get_tree().set_input_as_handled()
	if event.is_action_pressed("down"):
        adjustCursorIndex(1)
	elif event.is_action_pressed("up"):
        adjustCursorIndex(-1)
	elif event.is_action_pressed("a"):
        selectItemAtCursor()
	elif event.is_action_pressed("b"):
		emit_signal("menu_item_selected", "back", null)

func adjustCursorIndex(amt):
	print("x: " + str(cursorSprite.position.x) + "y: " + str(cursorSprite.position.y))
	var size = containerNode.get_child_count()
	cursorIndex += amt
	if(cursorIndex >= size):
		cursorIndex = 0
	elif(cursorIndex < 0):
		cursorIndex = size-1
	cursorSprite.position.y = (cursorIndex+ 1) * 8
	cursorSprite.position.x = 0
	print("x: " + str(cursorSprite.position.x) + "y: " + str(cursorSprite.position.y))

func selectItemAtCursor():
	var labelNode = containerNode.get_child(cursorIndex)
	print("menu item selected.  command=" + labelNode.command)
	emit_signal("menu_item_selected", labelNode.command, labelNode)

func printInfo(node):
	print(node.name + ": size: " + str(node.rect_size) + " loc: " + str(node.rect_position) + " marg: " + str(node.margin_top))
	
func printNodeTreeInfo(node):
	printInfo(node)
	for n in node.get_children():
		printNodeTreeInfo(n)

func _on_PanelContainer_resized():
	if(cursorSprite != null):
		cursorSprite.position.y = (cursorIndex+ 1) * 8
		cursorSprite.position.x = 0 # Replace with function body.
