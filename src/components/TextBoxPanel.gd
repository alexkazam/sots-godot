extends PanelContainer

const STATE_PRINT = 0;
const STATE_WAIT = 1;

onready var label = $Label

var charPerSec = 40
var charDelta = 1.0/charPerSec
var accum = 0
var characterBreakpointArray = [];
var state;
var page = -1;
var fullText;
var fullTextIndex = 0;
var pageIndex = 0;

var maxCharacterCount
var nextCharacterBreakpoint


func _ready():
	#maxCharacterCount = label.rect_size.x * label.rect_size.y / 8 / 8;
	label.visible_characters = 0;
	label.text = fullText
	nextPage()

func init(position:Vector2, width, height, t):
	self.rect_position = position
	self.rect_size.x = width
	self.rect_size.y = height
	maxCharacterCount = (width - 16) * (height - 16) / 8 / 8;
	fullText = t

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(state == STATE_PRINT):
		accum += delta
		if(accum >= charDelta):
			accum -= charDelta 
			label.visible_characters += 1
			if(label.visible_characters >= maxCharacterCount):
				state = STATE_WAIT
			#print("vis%:" + str(label.percent_visible) + ", line_count:" + str(label.get_line_count()) + ", vis_line_sount:" + str(label.get_visible_line_count()) )
		

func _unhandled_input(event):
	if event.is_action_pressed("a") && state == STATE_WAIT:
		nextPage();
	get_tree().set_input_as_handled()

func nextPage():
	page += 1
	label.lines_skipped = label.get_visible_line_count() * page
	if label.lines_skipped >= label.get_line_count():
		queue_free()
	label.visible_characters = 0;
	nextCharacterBreakpoint = characterBreakpointArray.pop_front()
	state = STATE_PRINT
